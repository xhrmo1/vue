import Dude from "./components/Dude.vue"

const app = Vue.createApp({
	data() {
		return {
			newDude: "",
			characters: ["Jake the Dosg", "Finn the Human", "Marceline the Vampire"],
		};
	},

});

app.component("dude", Dude);

app.mount("#app");

/*
<slot></slot> vloži mi html element do componenty čiže všetko čo je pod tagmi našej componenty 
to čo je v <slot></slot> je defaultna hodnota potom sa vymaže
ak je pred atributom dvojbodka, tak to v "" je brane ako javascript, dobre napr na vloženie premennej a nie textu
*/